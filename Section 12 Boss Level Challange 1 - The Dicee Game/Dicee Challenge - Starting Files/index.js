
//1st random number 1-6

var randomNumber1 = Math.floor( Math.random() * 6) + 1;

// replacing the 1st image to a random one

var randomImageSource1 =  "images/dice" + randomNumber1 + ".png"

document.querySelector(".img1").setAttribute("src", randomImageSource1);


//2st random number 1-6

var randomNumber2 = Math.floor( Math.random() * 6) + 1;

// Replacing the 2nd image to a random one

var randomImageSource2 = "images/dice" + randomNumber2 + ".png"

document.querySelector(".img2").setAttribute("src", randomImageSource2);

//Checking which player won and printing out the result

var headingReference = document.querySelector("h1");

if ( randomNumber1 > randomNumber2 ){

        headingReference.innerHTML="<i class='far fa-flag flag'></i> Player One Wins!";

}else if ( randomNumber1 < randomNumber2 ){

        headingReference.innerHTML="Player Two Wins! <i class='far fa-flag flag'></i> ";

}else{

        headingReference.innerHTML="Draw!";

}