import React from "react";
import ReactDOM from "react-dom";
import animals, {useAnimals} from "./data";
import cars from "./practice";

const [honda, tesla] = cars;

const { coloursByPopularity :  [hondaTopColour] , speedStats:{topSpeed : hondaTopSpeed}} = honda

const { coloursByPopularity: [teslaTopColour], speedStats:{topSpeed : teslaTopSpeed}} = tesla



console.log(honda);

ReactDOM.render(
    <table>
      <tr>
        <th>Brand</th>
        <th>Top Speed</th>
      </tr>
      <tr>
        <td>{tesla.model}</td>
        <td>{teslaTopSpeed}</td>
        <td>{teslaTopColour}</td>
      </tr>
      <tr>
        <td>{honda.model}</td> 
        <td>{hondaTopSpeed}</td>
        <td>{hondaTopColour}</td>
      </tr>
    </table>,
    document.getElementById("root")
  );


// Destructuring Arrays
// const [cat, dog] = animals;

//Destructuring Objects
// const {name, sound} = cat;

// const {name : catName, sound: catSound} = cat;

// const {name ="Fluffy", sound = "Purr"} = cat

// const {name, sound, feedingRequirements: {food, water}} = cat;

// console.log(food);

//Deconstructing objects and functions
// const [animal, makeSound] = useAnimals(cat);

// console.log(animal);
// makeSound();



// CHALLENGE: uncomment the code below and see the car stats rendered


