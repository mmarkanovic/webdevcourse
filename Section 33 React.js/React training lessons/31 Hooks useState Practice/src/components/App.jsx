import React from "react";



function App() {

  let currentTime = new Date().toLocaleTimeString();

  const [time, getTime] = React.useState(currentTime);



  function getCurrentTime (){
    getTime(new Date().toLocaleTimeString());
  }

  setInterval(getCurrentTime, 1000);

  return (
    <div className="container">
      <h1>{time}</h1>
      <button onClick={getCurrentTime}>Get Time</button>
    </div>
  );
}

export default App;
