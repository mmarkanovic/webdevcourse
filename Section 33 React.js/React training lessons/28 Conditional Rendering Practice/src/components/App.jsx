import React from "react";
import Form from "./Form";
import Login from "./Login";

var userIsRegistered = false;

function App() {
  return (
    <div className="container">
      {<Form isRegistered={userIsRegistered} />}
    </div>
  );
}

export default App;
