import React from "react";
import AddIcon from '@material-ui/icons/Add';
import Fab from "@material-ui/core/Fab";
import Zoom from "@material-ui/core/Zoom";

function CreateArea(props) {

    const [isExpanded, setExpanded] = React.useState(false);

    const [note, setNote] = React.useState({
        title: "",
        content: ""
    })

    function handleOnChange(event){
        const {name, value} = event.target;

        setNote(previousNote => {
            return {
                ...previousNote,
                [name]: value
            };
        });
    }

    function submitNote(event){
        props.onAdd(note);
        event.preventDefault();
        setNote({
            title:"",
            content:""
        });
    }

    function expand(){
        setExpanded(true);
    }

    return (
        <div>
        <form className="create-note">
            {isExpanded && (
                <input 
                name="title" 
                placeholder="Title" 
                value={note.title}
                onChange={handleOnChange}
                />
                )
            }
            <textarea 
                name="content" 
                placeholder="Take a note..." 
                rows={isExpanded ? 3 : 1} 
                value = {note.content}
                onChange={handleOnChange}
                onClick={expand}
            />
            <Zoom in={isExpanded}>
                <Fab onClick={submitNote}><AddIcon/></Fab>
            </Zoom>
        </form>
        </div>
    );
}

export default CreateArea;
