import React from "react";





function App() {

    const [headingText, setHeadingText] = React.useState("Hello");

    function handleClick(){
        setHeadingText("Submitted");
    }

    const [isMousedOver, setMouseOver] = React.useState(false);

    function handleMouseOver(){
        setMouseOver(true);
    }

    function handleMouseOut(){
        setMouseOver(false);
    }


  return (
    <div className="container">
      <h1>{headingText}</h1>
      <input type="text" placeholder="What's your name?" />
      <button 
        style={{backgroundColor: isMousedOver ? "black" : "white"}} 
        onClick={handleClick}
        onMouseOver={handleMouseOver} 
        onMouseOut={handleMouseOut}
        >
        Submit
        </button>
    </div>
  );
}

export default App;
