import React from "react";

function App() {

    const [toDoList, addNewItem] =React.useState([]);

    const [item, setItem] = React.useState("");

    

    function handleChange(event){
        const value = event.target.value;

        setItem(value);

    }

    function handleClick(){

        addNewItem(previousItems => {
            return [...previousItems, item];
        });

        setItem("");
        
    }
    
  return (
    <div className="container">
      <div className="heading">
        <h1>To-Do List</h1>
      </div>
      <div className="form">
        <input 
            type="text" 
            name="newItem"
            placeholder="enter a task"
            onChange={handleChange}
            value={item}
            />
        <button >
          <span onClick={handleClick}>Add</span>
        </button>
      </div>
      <div>
        <ul>
          {toDoList.map((toDoItem, i) => <li key={i}>{toDoItem}</li>)}
        </ul>
      </div>
    </div>
  );
}

export default App;

//CHALLENGE: Make this app work by applying what you've learnt.
//1. When new text is written into the input, its state should be saved.
//2. When the add button is pressed, the current data in the input should be
//added to an array.
//3. The <ul> should display all the array items as <li>s
