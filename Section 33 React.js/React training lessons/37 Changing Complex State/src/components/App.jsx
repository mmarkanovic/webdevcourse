import React from "react";

function App() {

    const [fullName, setFullName] = React.useState({
        firstName: "",
        lastName: ""
    });

    function handleChange(event){

        const {value, name} = event.target

        setFullName((prevValue) => {
            if(name === "firstName"){
                return{
                    firstName: value,
                    lastName: prevValue.lastName
                }
            }else if (name === "lastName"){
                return{
                    firstName: prevValue.firstName,
                    lastName: value
                }
            }
        });
    }

    return (
        <div className="container">
        <h1>Hello {fullName.firstName} {fullName.lastName} </h1>
        <form>
            <input 
                name="firstName" 
                placeholder="First Name" 
                onChange = {handleChange}
                value={fullName.fistName}
            />
            <input 
                name="lastName" 
                placeholder="Last Name" 
                onChange= {handleChange}
                value ={fullName.lastName}
            />
            <button>Submit</button>
        </form>
        </div>
    );
}

export default App;
