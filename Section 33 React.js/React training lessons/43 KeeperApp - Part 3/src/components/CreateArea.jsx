import React from "react";

function CreateArea(props) {

    const [note, setNote] = React.useState({
        title: "",
        content: ""
    })

    function handleOnChange(event){
        const {name, value} = event.target;

        setNote(previousNote => {
            return {
                ...previousNote,
                [name]: value
            };
        });
    }

    function submitNote(event){
        props.onAdd(note);
        event.preventDefault();
        setNote({
            title:"",
            content:""
        });
    }

    return (
        <div>
        <form >
            <input 
                name="title" 
                placeholder="Title" 
                value={note.title}
                onChange={handleOnChange}
            />
            <textarea 
                name="content" 
                placeholder="Take a note..." 
                rows="3" 
                value = {note.content}
                onChange={handleOnChange}
            />
            <button onClick={submitNote}>Add</button>
        </form>
        </div>
    );
}

export default CreateArea;
