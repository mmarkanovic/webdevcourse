import React from "react";

function App() {

    const [name, setName] = React.useState("");

    const [headingText, setHeading] = React.useState("");


    function handleChange(event){
        console.log(event.target.value);
        setName(event.target.value)
    }

    function clickedButton(event){
        setHeading(name);

        event.preventDefault();
    }
        
    return (
        <div className="container">
        <h1>Hello {headingText}</h1>
            <form onSubmit={clickedButton}>
                <input 
                    type="text" 
                    placeholder="What's your name?" 
                    onChange={handleChange}
                    value={name}    
                />
                <button type="submit">Submit</button>
            </form>
        </div>
    );
    }

export default App;
