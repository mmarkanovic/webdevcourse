import React, { useState } from "react";

function App() {
  const [contact, setContact] = useState({
    firstName: "",
    lastName: "",
    email: ""
  });

  function handleChange(event){
      
    const {value, name} = event.target;

    setContact((previousValue) => {
        return{
            ...previousValue,
            [name]: value
        }
    });

  }

  return (
    <div className="container">
      <h1>
        Hello {contact.firstName} {contact.lastName}
      </h1>
      <p>{contact.email}</p>
      <form>
        <input 
            name="firstName" 
            placeholder="First Name" 
            onChange={handleChange}
            value={contact.firstName}
        />
        <input 
            name="lastName" 
            placeholder="Last Name" 
            onChange={handleChange}
            value={contact.lastName}
        />
        <input 
            name="email" 
            placeholder="Email" 
            onChange={handleChange}
            value={contact.email}
        />
        <button>Submit</button>
      </form>
    </div>
  );
}

export default App;
