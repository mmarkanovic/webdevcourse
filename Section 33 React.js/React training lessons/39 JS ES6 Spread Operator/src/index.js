import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

ReactDOM.render(<App />, document.getElementById("root"));


// const citrus = ["Lime", "Lemon", "Orange"];
// const fruits = ["Apple", ...citrus, "Banana", "Coconut"];

// const fullName ={
//     firstName: "James",
//     lastName: "Bond"
// };

// const user ={
//     ...fullName,
//     id: 1,
//     username:"jamesbond007"
// };

// console.log(user);