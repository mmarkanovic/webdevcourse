import React from "react";
import Footer from "./Footer";
import Header from "./Header";
import Note from "./Note";
import Notes from "../Notes";




function App() {
    return (
        <div>
            <Header/>
            
            {Notes.map(Notes =>
                
                <Note
                    key={Notes.id}
                    title={Notes.title}
                    content={Notes.content}
                />
                
            )}

            <Footer/>
        </div>
    );

}

export default App;