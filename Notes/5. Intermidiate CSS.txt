Lesson 1 - What are Favicons?

	Favicons ili Favorite icon, ikona koja se pokaze kad netko bookmarka stranicu ili u danasnjim browserima pokaze se u lijevom dijelu otvorenog taba
	
	favicon.cc - site za pravljenje jednostavnih ikona

Lesson 2 - HTML Divs

	DIV je HTML oznaka za podjelu i raspoređivanje elemenata na stranici
	
	bitno je koristiti inspect element na stranici kako bi vijeli koje su vrijednosti defaultno postavljene kako bi ih mogli overraid-ati i napraviti izgled po nasim zeljama
	
	mozemo za testiranje mijenjati kod u inspect elementu te vidjeti sto moramo promijeniti kako bi dobili zeljeni izgled

Lesson 3 - The Box Model of Website Styling

	CSS sve elemente vidi kao kutije, i.e. box model
	
	i sa tim znanjem mozemo oblikovati te kutije kako zelimo
	
	vrijednosti mozemo postavljati u pikselima ili u % sto ce ovisiti o uređajima koje koristimo
	
	margin|border|padding|content|padding|border|margin
	
	margin dodaje razmak izmedu elemenata i ne prosiruje ih
	
	border dodaje granicu elementu te se siri prema van, tj poveca velicinu elementu
	
	padding dodaje granicu, tj prostor izmedu bordera i contenta te prosiruje podrucje koje element zauzima
	
	ne zaboravit obje strane (lijevo-desno, gore-dolje) kod zbrajanja za udaljenosti
	
Lesson 4 - CSS Display Property

	block elementi su elementi koji zauzimaju cijelu sirinu stranice, tj blokiraju ostalim elementima da budu pokraj njih  
	
	<span></span> HTML tag koji nam dopušta da odaberemo dio teksta, poznat je kao inline display element, tj zauzima mjesta koliko je potrebno za taj element, tj wrap_content
		span elementima ne mozemo mijenjati sirinu 
		
	blok elementima i span elementima mozemo mijenjati display svojsta, u block, inline, i inline-block
	
	
Lesson 5 - CSS Static and Relative Positioning

	Stranica ima default vrijednosti 
	
	1. Content is everything - tipa kod h1 taga on je block element znaci da ce mu sirina biti preko cijele stranice, ali visinu odreduje tekst koji se nalazi u njemu
	
	2. Order Comes from code - kod odreduje redoslijed slaganja elemenata
	
	3. Children sit on Parents - elementi koji se nalaze u necemu bit ce iznad toga po z osi na stranici, djeca su ispred roditelja, tj elementi koji su nested u necemu ce biti ispred tog neceg
	
	Position, pozicija 	- Static - svi HTML defaultno staticni, stoje na mjestu gdje ih stavimo
						- Relative - mjesto reference je gdje bi staticki element trebao biti
						
							koordinate 	- top, bottom, left, tight - ako kazemo da je top 100px pomaci cemo taj element 100px prema dolje, ako je left 100 px element ce se pomaci u desno 100px
										- kada micemo sa kordinatama taj element koji se pomice je neovisan o drugima, tj nece pomaci ostale elemente nego kao da se izdigne iznad njih i pređe preko njih
										
								rijesenje zadatka sa 3 bloka 
								
								HTML: 	<body>
										  <div class="red"></div>
										  <div class="blue"></div>
										  <div class="yellow"></div>
									  
									  
										</body>
									CSS:.red{
										  display: inline-block;
										  height: 100px;
										  width: 100px;
										  background-color:red;
										  position: relative;
										  left: 200px;
										}

										.blue{
										  display: inline-block;
										  height: 100px;
										  width: 100px;
										  background-color:blue;
										  position:relative;
										  right: 100px;
										}
										.yellow{
										  display: inline-block;
										  height: 100px;
										  width: 100px;
										  background-color:yellow;
										  position:relative;
										  right: 100px;
										}
										
Lesson 6 - Absolute Positioning

	Position, pozicija - Absolute 	- postavlja margin relativno roditelju tog elementa
									- kada jedan element stavimo absolute kao da vise ne postoji za druge elemente, tj ako ga postavimo absolute neki drugi element ce zauzeti njegovo mjesto, a taj element koji je absolute ce biti preko njega, tj iznad njega po z osi
									
									Challenge 1  solution
									
										.red{
										  height: 100px;
										  width: 100px;
										  background-color:red;
										  position: absolute;
										  left: 200px;
										  top: 200px;
										}

										.blue{
										  height: 100px;
										  width: 100px;
										  background-color:blue;
										  position: absolute;
										  left: 100px;
										  top: 100px;
										 
										}
										.yellow{
										  height: 100px;
										  width: 100px;
										  background-color:yellow;
										  position:absolute;
										  top:0;
										  left:0;
										}

							- Fixed - element ostaje na istom mjestu relativno na body od stranice, tipa kad scrollamo taj element ostaje na istom mjestu
							
Lesson 7 - The Dark Art of Centering Elements with CSS

	text-align: center; - postavlja inline i block elemente u sredinu stranice
	
	ako postavimo sirinu elementa neku određenu onda text align ne radi te onda koristimo maring auto
	
Lesson 8 - Font Styling in Our Personal Site

	font-family atribut u css dokumentu odreduje font 
	
	web safe fonts - fontovi koji ce najvjerovatnije svi imati instalirano
	
	cssfontstack.com - stranica za  provjeru postotaka fontova
	fonts.google.com - stranica sa free to use fontovima i mozemo iz izabrati te ih dodati na nasu stranicu da ih svi mogu vidjeti, tj da svi imaju te fontove koje smo zamislili
		dodamo link i onda u css postavljamo font-family
	
Lesson 9 - Adding Content to Our Website

	flaticon.com - stranica sa ikonama za dodat na stranicu ako nam se koja svida
	giphy.com - za gifove

Lesson 10 - CSS Sizing

	font-size s ovom naredbom u cssu mijenjamo velicinu slova teksta na stranici, kao u wordu da mijenjamo font-size
	
	100% je 16 px znaci da dobijemo 90 px, 90/16=5,625 * 100 = 562,5%
	
	1em (M) je 16px ili 100% 90px/16px=5,625em
	
	font koji je postavljen sa % ili sa em je dinamičan, tj velicina fonta ce se mijenjat zavisno o preferencama
	
	font-size kada koristimo em ili % font-size fontovi se zbrajaju, tj nasljeduju
	
	u CSS3 imamo rem oznaku ili rootem s kojim se velicina ne nasljeduje
	
	preporuka je koristiti rem umjesto svih ostalih zbog lakseg debuggiranja
	
	chalange h2 40px u rem, 40/16=2,5rem

Challenge 1 Change the Font colour - color: nesto
Challenge 2 Change the Font Weight - font-weight : normal
Challenge 3 Change the Font Height - line-height : 2 

Lesson 11 - Float and Clear - 

	Float se korisiti kada na primjer želimo omotat tekst oko slike, tj postaviti tekst na jednu od strana slike
	
	
	Clear se korisiti kada želimo da tekst koji smo prijašnje omotali oko slike da taj tekst ne može biti floatan
	
	Koristiti float samo za wrapanje teksta oko slike, ne za pozicioniranje stvari
	
	

CSS Challenge - ez zadatak, samo treba dobro procjenit vrijednosti koje ne pišu
