
//Task 1: promijeniti 3. točku u listu u svoje ime

document.querySelector("ul").lastElementChild.innerHTML = "Marko Markanović";

//task 2: promijeniti boju drugog google linka u crvenu

document.querySelector("#list a").style.color = "red";

// task 3 : promijeniti pozadinsku boju gumba na stranici u zutu (yellow) koristeci samo JS

document.querySelector(".btn").style.backgroundColor = "yellow"

// task 4: dodati klasu .huge u CSS i pomocu JS dodati tu klasu h1 elementu na stranici

document.querySelector("h1").classList.add("huge");

