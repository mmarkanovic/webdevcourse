Lesson 1 - Databases Explained: SQL cs. NOSQL

    Do sada kad smo radili nismo mogli dugorocno spremiti podatke, svaki put kada bi resetirali server nasi podatci bi se obrisali i morali bi ih ponovo unositi

    SQL vs NOSQL

    SQL - structured query language

        MySql, PostgradeSQL

    NOSQL - Not only structured query language

        mongoDB, redis

    razlike
        -Struktura

            za sve podatke u SQL bazi treba postojati predvideno mjesto, npr. ako skupljamo neke podatke i ako nam netko ne zeli dati svoje kontakt podatke imat cemo prazna polja te ce na tim poljima biti null i ako saljemo email na null moze doci do problema

            ovaj problem se moze lagano rijesiti sa NOSQL bazom podataka jer za svaki imamo varijablu, tj route do tog podataka te laske mozemo postaviti uvjet kad nemamo neki podatak

        - Relacije (relationships)

            Relacijske i neRelacijske

            Relacijske tablice mozemo povezivati tablice pomocu foreign keyeva i ID-eva te tako mozemo kombinirati podatke i na taj nacin smanjiti ponavljanje podataka

            dok kod noSQL baza moze doci do ponavljanja podataka

    
    ako trebamo spremati nesto sto ce imati puno relacija bolje je da koristimo SQL bazu

    ako imamo potrebu spremati nesto sto ima jedan na mnogo relaciju onda nam je bolje koristiti noSQL, npr. profil za instagram, jedan user ce imati puno postova

        -skalabilnost (scalability)

        teze je nadodavati stvari u SQL baze nego u noSQL baze

            npr. SQL je kao da gradimo zgradu i dodajemo katove kako bi prosirili mjesto, dok je koristenje noSQL baza sirenje u horizontalu, tj distribuira se tezina poslova

    za koristenje SQL baza moramo prije toga smisliti kako cemo podijeliti podatke, dok je noSQL baza fleksibilnija jer se podatci spremaju unutar dokumenata
