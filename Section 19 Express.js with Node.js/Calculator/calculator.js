const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.urlencoded({extended: true}));

//BMI Calculator 

app.get("/bmicalculator", function(req, res){
    res.sendFile(__dirname+"/bmiCalculator.html");
});

app.post("/bmicalculator", function(req, res){

    var height = parseFloat(req.body.height);
    var weight = parseFloat(req.body.weight);

    var resultBMI = ( weight / ( height * height ));

    var roundedBMI = Math.round((resultBMI + Number.EPSILON) * 100) / 100;

    res.send("Your BMi is " + roundedBMI);

});

//Calculator

app.get("/", function(req, res){
    res.sendFile(__dirname + "/index.html");
});

app.post("/", function(req, res){
    
    var num1 = Number(req.body.num1);
    var num2 = Number(req.body.num2);

    var result = num1 + num2;

    res.send("The result of the calculation is " + result);
});



app.listen(3000, function(){
    console.log("Server started on port 3000");
});