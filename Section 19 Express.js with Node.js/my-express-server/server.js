//jshint esversion:6

const express = require('express');

const app = express();

app.get("/", function(req, res){
    res.send("<h1>Hello world</h1>");
});

app.get("/contact", function(req, res){
    res.send("Contact me at: mmarkanovic13@gmail.com");
});

app.get("/about", function(req, res){
    res.send("I am Marko. I just finished my bachelor's degree and now I am learning about web dev so I can make my self some web pages ")
});

app.get("/hobbies", function(req, res){
    res.send("Kettelbells, F1, driving, cars ");
});

app.listen(3000, function(){
    console.log("Server started on port 3000");
});



