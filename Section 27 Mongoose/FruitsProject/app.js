const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/fruitsDB", {useNewUrlParser: true, useUnifiedTopology: true});


const fruitSchema = new mongoose.Schema ({
    name: {
        type: String,
        required: [true,"Please check your data entery, no name specified!"]
    }, 
    rating: {
        type: Number,
        min: 1,
        max: 10
    },
    review: String
});

const Fruit = mongoose.model("Fruit", fruitSchema);

const fruit = new Fruit({
    name: "Peach",
    rating: 7,
    review: "Pretty solid as a fruit."
});

// fruit.save();

//people

const peopleSchema = new mongoose.Schema({
    name: String,
    age: Number,
    favoriteFruit: fruitSchema
});

const People = mongoose.model("People", peopleSchema);

const mango = new Fruit({
    name: "Mango",
    rating: 6,
    review: "Decent fruit."
})

mango.save();

// const people = new People({
//     name: "Ana",
//     age: 12, 
//     favoriteFruit: pineapple
// });

// people.save();

People.updateOne(
    {name: "Marko"},
    {favoriteFruit:mango},
    function(err){
        if(err){
            console.log(err);
        } else{
            console.log("Successfully updated the document");
        }
    }
    );

Fruit.find(function(err, fruits){

    if(err){
        console.log(err);
    } else {

        

        fruits.forEach(function(fruit){
            console.log(fruit.name);
        });
        mongoose.connection.close();
    }


});


// Fruit.updateOne({
//     _id: "5fe49e12dda76c2d5c8e349a"
//     },
//     {
//         name: "Peach",
//         rating: 10,
//         review: "Best fruit ever"
//     },
//     function(err){
//         if(err){
//             console.log(err);
//         }else{
//             console.log("Successfully updated the document");
//         }
//     }
// );

// Fruit.deleteOne({
//     _id: "5fe49f94584c243070f6d34f"
//     },
//     function(err){
//         if(err){
//             console.log(err);
//         }else{
//             console.log("Successfully deleted the document");
//         }
//     }
// );







  // const kiwi = new Fruit({
//     name: "Kiwi", 
//     score: 6,
//     review: "Cool Fruit"
// });

// const orange = new Fruit({
//     name: "Orange", 
//     score: 7,
//     review: "Can be great"
// });

// const banana = new Fruit({
//     name: "Banana", 
//     score: 8,
//     review: "Great, but sometimes too sweet"
// });

// Fruit.insertMany([kiwi, orange, banana], function(err){
//     if (err){
//         console.log(err);
//     } else{
//         console.log("Successfully saved all the fruits to fruitsDB");
//     }
// });